
const router = require('express').Router();
import User from '../model/User';
import { sign } from 'jsonwebtoken';
import { genSalt, hash, compare } from 'bcryptjs';
import { registerValidation, loginValidation } from '../lib/validation';


router.post('/register', async (req, res) => {
    // lets validate the data before we a user 
    const {error} = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // Checking if the user is already in the database 
    const emailExist = await User.findOne({ email: req.body.email });
    if (emailExist) return res.status(400).send('Email already exits');

    // hash passwords 
    const salt = await genSalt(10);
    const hashedPassword = await hash(req.body.password, salt);

    // Create a new user 
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
    });
    try {
        await user.save();
        res.send({
            user: user._id});
    } catch (err) {
        res.status(400).send(err);
    }
});

router.post('/login', async (req, res) => {
    // lets validate the data before we a user 
    const {error} = loginValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // Checking if the email exits
    const user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(400).send('Email is not found');
    // PASSWORD IS CORRECT 
    const validPass = await compare(req.body.password, user.password);
    if(!validPass) return res.status(400).send('Invalid Password');

    // Create and assign a token 
    const token = sign({_id: user._id}, process.env.TOKEN_SECREC);
    res.header('auth-token', token).send(token);
});

export default router;
