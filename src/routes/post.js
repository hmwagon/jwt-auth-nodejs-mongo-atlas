const router = require('express').Router();
const verify = require('./verifyToken').default;

router.get('/', verify, (req, res) => {
    res.json({posts: {title: 'my first post', description: 'other more'}})
});

module.exports = router;
