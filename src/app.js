import express from 'express';
const app = express();
import dotenv from 'dotenv';

// import routes
import authRoute from './routes/auth'
import postRoute from './routes/post'

dotenv.config();

// Middleware
app.use(express.json());

// Route middlewares
app.use('/api/user', authRoute);
app.use('/api/posts', postRoute);

export default  app