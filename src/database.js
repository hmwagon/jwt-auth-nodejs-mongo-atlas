import { connect } from 'mongoose';

//connect to DB
    connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(db => console.log(`Database is connected`));