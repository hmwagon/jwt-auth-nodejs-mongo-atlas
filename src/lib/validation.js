// validation JOI
import Joi from 'joi';


// Register Validation 
let registerValidation = (data) => {
    let schema = Joi.object({
        name: Joi.string().min(6).required(),
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

    });
    return schema.validate(data);
}
// Login Validation 
let loginValidation = (data) => {
    let schema = Joi.object({
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

    });
    return schema.validate(data);
}

let _registerValidation = registerValidation;
export { _registerValidation as registerValidation };
let _loginValidation = loginValidation;
export { _loginValidation as loginValidation };
