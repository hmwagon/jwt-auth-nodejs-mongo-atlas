import app from './app';
import './database';

async function init() {
    await app.listen(process.env.PORT, () => console.log(`Server on port ${process.env.PORT}`));
}

init();